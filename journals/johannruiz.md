# Hannah Lyons Daily Log / Journal

## Week 2

| Date | Effort/Results | Time Spent |
| :--- | :--- | :--- |
| May 29, 2024 | Met with team and came up with several project ideas | 1 hour |
| May 29, 2024 | Met with professor and proposed project ideas / finalized project idea | 1 hour |
| May 30, 2024 | Divided up Project Definition tasking | 10 minutes |
| May 31, 2024 | Divided up Project Definition tasking more | 10 minutes |
| June 1, 2024 | Completed my portion of Project Definition tasking | 2 hours |
| June 2, 2024 | Revised Project Definition with team before submitting | 1.5 hours |

## Week 3

| Date | Effort/Results | Time Spent |
| :--- | :--- | :--- |
| June 3, 2024 | Discussed plan for Sprint 1 | 10 minutes |
| June 4, 2024 | Met with team to create shared <a href="https://git.cs.vt.edu/cscapstone-group3">repositories</a> and discuss Sprint plans | 15 minutes |
| June 4, 2024 | Met with professor and received feedback | 30 minutes |
| June 4, 2024 | Created and shared <a href="https://www.figma.com/team_invite/redeem/3myWHIqlRj2y3q3txgfXMD">Gitlab</a> repositories for the frontend, backend, and documentation  | 5 minutes |
| June 4, 2024 | Met with team to create Product Backlog in <a href="https://vtmeng-cscapstone-health.atlassian.net/jira/software/projects/VRC/boards/2">jira</a> | 1 hour |
| June 4, 2024 | Created sample react app and dockerfile in <a href="https://git.cs.vt.edu/cscapstone-group3/rc-frontend">frontend repository</a>| 15 minutes |
| June 6, 2024 | Created Dockerfile for react + typescript + vite and .gitlab-ci.yml pipeline file to push images into gitlab registry | 30 minutes |
| June 7, 2024 | Update Dockerfile and pipeline, also published website on <a href="https://vtresearchconnect.discovery.cs.vt.edu/">cs cloud</a> (Need to be connected to vpn)   | 1.5 hours


# Week 4

Date | Effort/Results | Time Spent |
| :--- | :--- | :--- |
| June 9, 2024  | Drafted initial design of database tables for lab, discussion, student, professor, and posting tables | 30 minutes |
| June 9, 2024  | Created Sprint 1 documenation, refined Product Backlog with definitions, and organized it in Sprints | 3 hours |
| June 10, 2024 | Moved stories for research lab and login feature to Sprint 2 backlog and started sprint | 10 minutes |
| June 11, 2024 | Created Dockerfile for Spring Boot application, created Docker Compose file to start postgres and spring boot in containers, created labs table and seeded with sample data for testing, created Model, Repository, and Controller to pull Lab information from the database and present it to web 
<img src="jrpics/labdata.png" alt="Lab Data" width="350" height="500"> 
<img src="jrpics/springanddbcontainer.png" alt="Spring Boot and Postgres Containers" width="1000" height="500"> | 2.5 hours



| June 11, 2024 | Created Dockerfile for postgres, pipeline for rc-backend to create postgres and backend container, and deployed both. Also ensured that the frontend can call the backend | 3 hours
