# Mahima Singh Daily Log / Journal

## Week 2

| Date         | Effort/Results                                                         | Time Spent |
|:-------------|:-----------------------------------------------------------------------|:-----------|
| May 29, 2024 | Thought about Project Ideas . Met with team and finalized project ideas                                                                                   | 2 hours     |
| May 29, 2024 | Met with professor and proposed project ideas / finalized project idea | 1 hour     |
| May 30, 2024 | Divided up Project Definition tasks                                    | 20 minutes |
| May 31, 2024 | Signed up to Gitlab/Trello/Jira to make user stories                   | 10 minutes |
| June 1, 2024 | Completed section 8-10 of Project Definition                           | 2 hours    |
| June 2, 2024 | Refined Project scope ,features and deliverables with team                                                                                    | 1.5 hours  |
| June 2, 2024 | Discussed Project Definition with team before submitting               | 1.5 hours  |

## Week 3

| Date         | Effort/Results                                                                                                                                                               | Time Spent |
|:-------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------|
| June 3, 2024 | Sprint Planning , Backlog Creation, Story creation 1                                                                                                                                                  | 10 minutes |
| June 4, 2024 | Met with team to create shared <a href="https://git.cs.vt.edu/cscapstone-group3">repositories</a> and discuss Sprint plans                                                   | 30 minutes |
| June 4, 2024 | Met with professor and received feedback                                                                                           | 30 minutes |
| June 4, 2024 | Met with team to finalized Sprint 1 tasking                                                                                                                                  | 1.5 hours  |
| June 7, 2024 | Started working on creating a sprinboot application with dummy website layout in Figma                                                                                                                                | 1 hour     |
| June 8, 2024 | Finished Springboot Application basic java src structure and integrated the application with React home pages                                                                                 | 1.5 hour     |
| June 9, 2024 | Checked in the code after testing into Gitlab into vtresearchconnect branch and sent a merge request for review to Johann                                                                                 | 1.5 hours    |
| June 9, 2024 | Updating Project Definition Document with feedback received                            | 1 hour    |