# Hannah Lyons Daily Log / Journal

## Week 2

| Date         | Effort/Results                                                         | Time Spent |
|:-------------|:-----------------------------------------------------------------------|:-----------|
| May 29, 2024 | Met with team and came up with several project ideas                   | 1 hour     |
| May 29, 2024 | Met with professor and proposed project ideas / finalized project idea | 1 hour     |
| May 30, 2024 | Divided up Project Definition tasking                                  | 10 minutes |
| May 31, 2024 | Divided up Project Definition tasking more                             | 10 minutes |
| June 1, 2024 | Completed my portion of Project Definition tasking                     | 2 hours    |
| June 2, 2024 | Revised Project Definition with team before submitting                 | 1.5 hours  |

## Week 3 (Sprint 1)

| Date         | Effort/Results                                                                                                             | Time Spent |
|:-------------|:---------------------------------------------------------------------------------------------------------------------------|:-----------|
| June 3, 2024 | Discussed plan for Sprint 1                                                                                                | 10 minutes |
| June 4, 2024 | Met with team to create shared <a href="https://git.cs.vt.edu/cscapstone-group3">repositories</a> and discuss Sprint plans | 15 minutes |
| June 4, 2024 | Met with professor and received feedbackMet with professor and received feedback                                           | 30 minutes |
| June 4, 2024 | Created and shared Figma project with team to collaborate on the design of our sites web pages                             | 5 minutes  |
| June 4, 2024 | Met with team to finalized Sprint 1 tasking                                                                                | 1.5 hours  |
| June 4, 2024 | Started header/footer website layout in Figma                                                                              | 1 hour     |
| June 5, 2024 | Spent time learning React, Typescript, and Vite, watched LinkedIn Learning videos                                          | 3 hours    |
| June 6, 2024 | Finished header/footer website layout and welcome page layout in Figma                                                     | 1 hour     |
| June 6, 2024 | Created React Project and coded AppHeader, AppFooter and first Route to WelcomePage                                        | 3 hours    |
| June 7, 2024 | Met with team to go over how to refresh images on CS cloud                                                                 | 1 hour     |
| June 8, 2024 | Updated src images for logo/background in React to display properly in CS cloud                                            | 10 minutes |
| June 9, 2024 | Created UML Use Case Diagram, User Personas, and User Stories. Completed Sprint 1 documentation with team.                 | 6 hours    |

## Week 4 (Sprint 2)

| Date           | Effort/Results                                                                                                                                                                                                                                                 | Time Spent |
|:---------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------|
| June 11, 2024  | Sprint 1 feedback meeting with professor & team                                                                                                                                                                                                                | 30 minutes |
| June 11,  2024 | Planned Sprint 2 with team                                                                                                                                                                                                                                     | 30 minutes |
| June 11, 2024  | Set Placeholders for all links on site (React Routes)                                                                                                                                                                                                          | 30 minutes |
| June 13, 2024  | Created Sign In, Sign Up (Professor), and Sign Up (Student) <a href="https://git.cs.vt.edu/cscapstone-group3/documentation/-/tree/main/Figma%20Pages?ref_type=heads">Figma Pages</a>                                                                           | 2 hours    |
| June 13, 2024  | Created <a href="https://git.cs.vt.edu/cscapstone-group3/documentation/-/blob/main/logo/logo.png?ref_type=heads">Logo</a> for Website using <a href="https://git.cs.vt.edu/cscapstone-group3/documentation/-/blob/main/logo/logo.py?ref_type=heads">Python</a> | 1 hour     |
| June 13, 2024  | Created Sign In React Page                                                                                                                                                                                                                                     | 1 hours    |
| June 14, 2024  | Created Sign Up (Professor) React Page                                                                                                                                                                                                                         | 1 hour     |
| June 14, 2024  | Created Sign Up (Student) React Page                                                                                                                                                                                                                           | 1 hour     |
| June 15, 2024  | Added Sign In functionality to communicate with backend                                                                                                                                                                                                        | 1 hour     |
| June 16, 2024  | troubleshoot connecting to backend docker container                                                                                                                                                                                                            | 1 hour     |
| June 17, 2024  | added util.ts file to front end with path names, and updated where those paths were called in the front end                                                                                                                                                    | 1 hour     |
| June 17, 2024  | finished Sprint 2 documentation with team                                                                                                                                                                                                                      | 1 hour     |

## Week 5 (Sprint 3)

| Date          | Effort/Results                                                                             | Time Spent |
|:--------------|:-------------------------------------------------------------------------------------------|:-----------|
| June 18, 2024 | Presented 1st Progress report to professor and class                                       | 30 minutes |
| June 18, 2024 | Planned Sprint 3 with team, focus on fixing login/logout and add research lab page         | 30 minutes |
| June 20, 2024 | Created Research Lab Figma Page                                                            | 1 hour     |
| June 20, 2024 | Spent time learning UseEffect / UseState in React                                          | 3 hour     |
| June 23, 2024 | Attempted to fix login/logout functionality in backend (i didnt figure it out, mahima did) | 1 hours    |
| June 24, 2024 | Created Research Lab React Page                                                            | 3 hour     |
| June 24, 2024 | Gathered Sprint 3 documentation with team for submission                                   | 2 hour     |

## Week 6 (Sprint 4)

| Date          | Effort/Results                                                    | Time Spent |
|:--------------|:------------------------------------------------------------------|:-----------|
| June 26, 2024 | Met with team to discuss Sprint 4 and plan out tasks for the week | 1 hour     |
| June 30, 2024 | Created Discussion Board Figma Page                               | 1 hour     |
| June 30, 2024 | Updated Lab Page Filtering CSS                                    | 1 hour     |
| June 30, 2024 | Created Discussion Board React Page                               | 3 hour     |
| July 1, 2024  | Gathered Sprint 4 documentation with team for submission          | 2 hour     |

## Week 7 (Sprint 5)

| Date         | Effort/Results                                                                 | Time Spent |
|:-------------|:-------------------------------------------------------------------------------|:-----------|
| July 2, 2024 | Sprint 4 feedback meeting with professor & team                                | 30 minutes |
| July 2, 2024 | Met with team to discuss Sprint 5 and plan out tasks for the week              | 1 hour     |
| July 4, 2024 | Created Context for fetching majors from the backend to display on signup page | 1.5 hours  |
| July 7, 2024 | troubleshoot connecting to backend docker container                            | 1 hour     |
| July 7, 2024 | Create Contexts for fetching Colleges, Discussion, Login, and Research Lab     | 2 hours    |
| July 7, 2024 | Bug fixing                                                                     | 2 hours    |

## Week 8 (Sprint 5 continued)

| Date          | Effort/Results                                                                                                | Time Spent |
|:--------------|:--------------------------------------------------------------------------------------------------------------|:-----------|
| July 8, 2024  | Create React Page for a single discussion                                                                     | 3 hours    |
| July 8, 2024  | Fix front end bugs - neater code, better error handling, and better user experience                           | 2 hours    |
| July 8, 2024  | Completed front end POST for Student Sign Up and for Professor Sign Up                                        | 2 hours    |
| July 9, 2024  | Worked on PowerPoint Presentation for Progress Presentation 2                                                 | 2 hours    |
| July 9, 2024  | Presented 2nd Progress report to professor and class                                                          | 30 minutes |
| July 11, 2024 | Use NavLinks instead of Links React component on Labs Page for better routing                                 | 1 hour     |
| July 11, 2024 | Created views for different roles                                                                             | 1 hour     |
| July 11, 2024 | Update backend login response to send the user's role to the front end, which is then stored in local storage | 3 hour     |
| July 13, 2024 | implemented adding a comment to a discussion                                                                  | 3 hours    |
| July 13, 2024 | implemented storing user email upon login to retrieve user details                                            | 3 hours    |
| July 14, 2024 | Gathered Sprint 5 documentation with team for submission                                                      | 2 hours    |

## Week 9 (Sprint 6)

| Date          | Effort/Results                                                                                                                          | Time Spent |
|:--------------|:----------------------------------------------------------------------------------------------------------------------------------------|:-----------|
| July 15, 2024 | Met with team to discuss Sprint 6 and plan out tasks for the week                                                                       | 1 hour     |
| July 16, 2024 | Met with professor and team to go over sprint 5                                                                                         | 30 minutes |
| July 19, 2024 | Update Login/Signup to use Formik/Yup for validation and better informayion messages                                                    | 6 hours    |
| July 19, 2024 | Added confirmation page upon signup completion                                                                                          | 1 hour     |
| July 19, 2024 | Implemented profile page for students and professors, links to profile available on discussion posts/comments                           | 3 hours    |
| July 19, 2024 | Added delete buttons for discussions and comments (based on user role, admin can delete anything, user can only delete their own posts) | 2 hours    |
| July 22, 2024 | Worked with team to finalize Sprint 6 documentation                                                                                     | 2 hours    |

## Week 10 (Sprint 7 - Final Sprint)

| Date          | Effort/Results                                                                                                                     | Time Spent |
|:--------------|:-----------------------------------------------------------------------------------------------------------------------------------|:-----------|
| July 22, 2024 | Met with team and professor to review sprint 6 and talk about plans for sprint 7                                                   | 30 minutes |
| July 23, 2024 | Met with team to discuss Sprint 7 and plan out tasks for the week                                                                  | 1 hour     |
| July 25, 2024 | Met with team to discuss specific tasking for the rest of the sprint                                                               | 30 minutes |
| July 26, 2024 | Created a Figma Page for Openings Page                                                                                             | 1 hour     |
| July 26, 2024 | update lab to get lab by ID, update posting to use lab id instead of labName string, update security config to add api/postings/** | 2 hours    |
| July 26, 2024 | implement openings page, implement add opening (if professor), implement opening popup                                             | 4 hours    |
| July 27, 2024 | update opening bugs on frontend                                                                                                    | 3 hours    |
| July 27, 2024 | AUTH checks for each page route - even if typed manually                                                                           | 1 hours    |
| July 28, 2024 | Worked with team to finalize Sprint 7 documentation                                                                                | 1 hour     |

## Week 11 (Final Presentation Preparation)

| Date           | Effort/Results                                                                                        | Time Spent |
|:---------------|:------------------------------------------------------------------------------------------------------|:-----------|
| July 30, 2024  | Met with team and professor to go over sprint 7 and discuss final presentation                        | 30 minutes |
| July 31, 2024  | Met with team to plan out tasks for the week to finish website, fix bugs, and create final powerpoint | 30 minutes |
| August 2, 2024 | Fixed discussion refresh on create                                                                    | 1 hour     |
| August 2, 2024 | Use session storage for UserContext instead of local storage                                          | 30 minutes |
| August 2, 2024 | Added better message for sign up confirmation page and updated confetti                               | 30 minutes |
| August 2, 2024 | Added refresh for labs, openings, and discussions on page click in header and footer                  | 1 hour     |
| August 2, 2024 | Added refresh for admin page on delete                                                                | 1 hour     |
| August 2, 2024 | filter majors by college on signup                                                                    | 1 hour     |
| August 3, 2024 | update createdAt on postings page                                                                     | 2 hours    |

## Week 12 (Final Presentation)

| Date           | Effort/Results                                                                                       | Time Spent |
|:---------------|:-----------------------------------------------------------------------------------------------------|:-----------|
| August 5, 2024 | update lab popup for professors and admin to see                                                     | 1 hour     |
| August 5, 2024 | fix delete discussion on item page to navigate back to discussion                                    | 1 hour     |
| August 5, 2024 | work on powerpoint presentation and practice our presentation as a team                              | 3 hours    |
| August 6, 2024 | edit information for professor appears in boxes, filtering by college click reverts back to all labs | 1 hour     |
| August 6, 2024 | removed others option, deleted comment updatesAdded better error handling for login/signup           | 1 hour     |
| August 6, 2024 | final bug fixes, finishing touches on presentation                                                   | 2 hours    |
| August 6, 2024 | Final Presentation to class and professor                                                            | 30 minutes |