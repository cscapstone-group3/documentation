import matplotlib.pyplot as plt
import numpy as np

theta = np.linspace(0, 2. * np.pi, 1000)
colors = ['white', '#861F41', '#E5751F', '#AB637A']
fig = plt.figure(figsize=(6, 6), dpi=300)
fig.set_facecolor('black')
ax = fig.add_subplot(111, polar=True)
for i, color in enumerate(colors):
    r = np.abs(np.sin((i + 2) * theta))
    ax.plot(theta, r, color=color, linewidth=2)
ax.set_title('')
ax.grid(False)
ax.set_yticklabels([])
ax.set_xticklabels([])
ax.spines['polar'].set_visible(False)
ax.set_facecolor('black')
plt.show()
